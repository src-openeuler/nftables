Name:           nftables
Version:        1.0.8
Release:        7
Epoch:          1
Summary:        A subsystem of the Linux kernel processing network data
License:        GPLv2
URL:            https://netfilter.org/projects/nftables/
Source0:        http://ftp.netfilter.org/pub/nftables/nftables-%{version}.tar.xz
Source1:        nftables.service
Source2:        nftables.conf

Patch0001:      create-standlone-module-dir.patch
Patch0002:      backport-exthdr-prefer-raw_type-instead-of-desc-type.patch
Patch0003:      backport-libnftables-Drop-cache-in-c-check-mode.patch
Patch0004:      backport-py-fix-exception-during-cleanup-of-half-initialized-.patch
Patch0005:      backport-evaluate-fix-check-for-truncation-in-stmt_evaluate_l.patch
Patch0006:      backport-evaluate-do-not-remove-anonymous-set-with-protocol-f.patch
Patch0007:      backport-evaluate-revisit-anonymous-set-with-single-element-o.patch
Patch0008:      backport-evaluate-skip-anonymous-set-optimization-for-concate.patch
Patch0009:      backport-datatype-fix-leak-and-cleanup-reference-counting-for.patch
Patch0010:      backport-evaluate-fix-memleak-in-prefix-evaluation-with-wildc.patch
Patch0011:      backport-netlink-fix-leaking-typeof_expr_data-typeof_expr_key.patch
Patch0012:      backport-datatype-initialize-TYPE_CT_LABEL-slot-in-datatype-a.patch
Patch0013:      backport-datatype-initialize-TYPE_CT_EVENTBIT-slot-in-datatyp.patch
Patch0014:      backport-netlink-handle-invalid-etype-in-set_make_key.patch
Patch0015:      backport-parser_json-Default-meter-size-to-zero.patch
Patch0016:      backport-parser_json-Fix-flowtable-prio-value-parsing.patch
Patch0017:      backport-parser_json-Proper-ct-expectation-attribute-parsing.patch
Patch0018:      backport-parser_json-Fix-synproxy-object-mss-wscale-parsing.patch
Patch0019:      backport-parser_json-Fix-typo-in-json_parse_cmd_add_object.patch
Patch0020:      backport-parser_json-Wrong-check-in-json_parse_ct_timeout_pol.patch
Patch0021:      backport-parser_json-Catch-nonsense-ops-in-match-statement.patch
Patch0022:      backport-json-expose-dynamic-flag.patch
Patch0023:      backport-evaluate-validate-maximum-log-statement-prefix-lengt.patch
Patch0024:      backport-evaluate-reject-set-in-concatenation.patch
Patch0025:      backport-datatype-don-t-return-a-const-string-from-cgroupv2_g.patch
Patch0026:      backport-json-fix-use-after-free-in-table_flags_json.patch
Patch0027:      backport-evaluate-fix-double-free-on-dtype-release.patch
Patch0028:      backport-evaluate-validate-chain-max-length.patch
Patch0029:      backport-parser_bison-fix-memleak-in-meta-set-error-handling.patch
Patch0030:      backport-parser_bison-make-sure-obj_free-releases-timeout-pol.patch
Patch0031:      backport-parser_bison-fix-ct-scope-underflow-if-ct-helper-sec.patch
Patch0032:      backport-evaluate-stmt_nat-set-reference-must-point-to-a-map.patch
Patch0033:      backport-meta-fix-tc-classid-parsing-out-of-bounds-access.patch
Patch0034:      backport-netlink-don-t-crash-if-prefix-for-byte-is-requested.patch
Patch0035:      backport-evaluate-don-t-crash-if-object-map-does-not-refer-to.patch
Patch0036:      backport-evaluate-error-out-when-expression-has-no-datatype.patch
Patch0037:      backport-evaluate-tproxy-move-range-error-checks-after-arg-ev.patch
Patch0038:      backport-evaluate-error-out-when-store-needs-more-than-one-12.patch
Patch0039:      backport-rule-fix-sym-refcount-assertion.patch
Patch0040:      backport-evaluate-guard-against-NULL-basetype.patch
Patch0041:      backport-evaluate-error-out-if-basetypes-are-different.patch
Patch0042:      backport-evaluate-reject-attempt-to-update-a-set.patch
Patch0043:      backport-evaluate-release-mpz-type-in-expr_evaluate_list-erro.patch
Patch0044:      backport-expression-missing-line-in-describe-command-with-inv.patch
Patch0045:      backport-evaluate-handle-invalid-mapping-expressions-graceful.patch

Patch0046:      backport-evaluate-disable-meta-set-with-ranges.patch
Patch0047:      backport-src-reject-large-raw-payload-and-concat-expressions.patch
Patch0048:      backport-evaluate-fix-stack-overflow-with-huge-priority-string.patch
Patch0049:      backport-tcpopt-don-t-create-exthdr-expression-without-datatype.patch
Patch0050:      backport-src-do-not-allow-to-chain-more-than-16-binops.patch
Patch0051:      backport-rule-fix-ASAN-errors-in-chain-priority-to-textual-names.patch
Patch0052:      backport-tests-shell-add-regression-test-for-double-free-crash-bug.patch
Patch0053:      backport-evaluate-handle-invalid-mapping-expressions-in-stateful-object-statements-gracefully.patch
Patch0054:      backport-evaluate-Fix-incorrect-checking-the-base-variable-in-case-of-IPV6.patch

Patch0055:      backport-ct-expectation-fix-list-object-x-vs-list-objects-in-table-confusion.patch
Patch0056:      backport-tests-shell-connect-chains-to-hook-point.patch

Patch0057:      backport-parser_json-release-buffer-returned-by-json_dumps.patch
Patch0058:      backport-parser_json-fix-crash-in-json_parse_set_stmt_list.patch
Patch0059:      backport-parser_json-fix-handle-memleak-from-error-path.patch
Patch0060:      backport-parser_json-fix-several-expression-memleaks-from-error-path.patch
Patch0061:      backport-libnftables-Zero-ctx-vars-after-freeing-it.patch

BuildRequires:  gcc flex bison libmnl-devel gmp-devel readline-devel libnftnl-devel docbook2X systemd
BuildRequires:  iptables-devel jansson-devel python3-devel
BuildRequires:  chrpath libedit-devel

%description
nftables is a subsystem of the Linux kernel providing filtering and classification of\
network packets/datagrams/frames. 

%package        devel
Summary:        Development library for nftables / libnftables
Requires:       %{name} = %{epoch}:%{version}-%{release}  pkgconfig

%description devel
Development tools and static libraries and header files for the libnftables library.

%package_help

%package -n     python3-nftables
Summary:        Python module providing an interface to libnftables
Requires:       %{name} = %{epoch}:%{version}-%{release}
%{?python_provide:%python_provide python3-nftables}

%description -n python3-nftables
The nftables python module providing an interface to libnftables via ctypes.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-silent-rules --with-xtables --with-json \
        --enable-python --with-python-bin=%{__python3}
%make_build

%check
make check

%install
export SETUPTOOLS_USE_DISTUTILS=stdlib
%make_install
%delete_la

chmod 644 $RPM_BUILD_ROOT/%{_mandir}/man8/nft*

install -d $RPM_BUILD_ROOT/%{_unitdir}
cp -a %{SOURCE1} $RPM_BUILD_ROOT/%{_unitdir}/

install -d $RPM_BUILD_ROOT/%{_sysconfdir}/sysconfig
cp -a %{SOURCE2} $RPM_BUILD_ROOT/%{_sysconfdir}/sysconfig/

install -d $RPM_BUILD_ROOT/%{_sysconfdir}/nftables
mv $RPM_BUILD_ROOT/%{_datadir}/nftables/*.nft $RPM_BUILD_ROOT/%{_sysconfdir}/nftables/

chrpath -d %{buildroot}%{_sbindir}/nft

mkdir -p %{buildroot}/etc/ld.so.conf.d
echo "%{_libdir}" > %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf

%post
%systemd_post nftables.service
/sbin/ldconfig

%preun
%systemd_preun nftables.service

%postun
%systemd_postun_with_restart nftables.service
/sbin/ldconfig

%ldconfig_scriptlets devel

%files
%defattr(-,root,root)
%license COPYING
%config(noreplace) %{_sysconfdir}/nftables/
%config(noreplace) %{_sysconfdir}/sysconfig/nftables.conf
%config(noreplace) /etc/ld.so.conf.d/*
%{_sbindir}/nft
%{_libdir}/*.so.*
%{_unitdir}/nftables.service
%{_docdir}/nftables/examples/*.nft

%files devel
%defattr(-,root,root)
%{_includedir}/nftables/libnftables.h
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%files help
%defattr(-,root,root)
%{_mandir}/man8/nft*
%{_mandir}/man3/libnftables.3*
%{_mandir}/man5/libnftables-json*

%files -n python3-nftables
%{python3_sitelib}/nftables-*.egg-info
%{python3_sitelib}/nftables/

%changelog
* Wed Dec 11 2024 gaihuiying <eaglegai@163.com> - 1:1.0.8-7
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:backport upstream patches
       parser_json: release buffer returned by json_dumps
       parser_json: fix crash in json_parse_set_stmt_list
       parser_json: fix handle memleak from error path
       parser_json: fix several expression memleaks from error path
       libnftables: Zero ctx->vars after freeing it

* Tue Dec 10 2024 gaihuiying <eaglegai@163.com> - 1:1.0.8-6
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:ct expectation: fix 'list object x' vs. 'list objects in table' confusion
       tests: shell: connect chains to hook point

* Wed Sep 25 2024 gaihuiying <eaglegai@163.com> - 1:1.0.8-5
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:backport upstream patches
       evaluate: disable meta set with ranges
       src: reject large raw payload and concat expressions
       evaluate: fix stack overflow with huge priority string
       tcpopt: don't create exthdr expression without datatype
       src: do not allow to chain more than 16 binops
       rule: fix ASAN errors in chain priority to textual names
       tests: shell: add regression test for double-free crash bug
       evaluate: handle invalid mapping expressions in stateful object
       evaluate: Fix incorrect checking the `base` variable in case of IPV6
 
* Mon Jun 24 2024 liweigang <liweiganga@uniontech.com> - 1:1.0.8-4
- Type: bugfix
- CVE: NA
- SUG: NA
- DESC: evaluate: guard against NULL basetype
evaluate: error out if basetypes are different
evaluate: reject attempt to update a set
evaluate: release mpz type in expr_evaluate_list() error path
expression: missing line in describe command with invalid expression
evaluate: handle invalid mapping expressions in stateful object statements gracefully

* Fri Apr 19 2024 lingsheng <lingsheng1@h-partners.com> - 1:1.0.8-3
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:datatype: don't return a const string from cgroupv2_get_path()
datatype: fix leak and cleanup reference counting for struct datatype
datatype: initialize TYPE_CT_EVENTBIT slot in datatype array
datatype: initialize TYPE_CT_LABEL slot in datatype array
evaluate: do not remove anonymous set with protocol flags and single element
evaluate: don't crash if object map does not refer to a value
evaluate: error out when expression has no datatype
evaluate: error out when store needs more than one 128bit register of align fixup
evaluate: fix check for truncation in stmt_evaluate_log_prefix()
evaluate: fix double free on dtype release
evaluate: fix memleak in prefix evaluation with wildcard interface name
evaluate: reject set in concatenation
evaluate: revisit anonymous set with single element optimization
evaluate: skip anonymous set optimization for concatenations
evaluate: stmt_nat: set reference must point to a map
evaluate: tproxy: move range error checks after arg evaluation
evaluate: validate chain max length
evaluate: validate maximum log statement prefix length
exthdr: prefer raw_type instead of desc->type
json: expose dynamic flag
json: fix use after free in table_flags_json()
libnftables: Drop cache in -c/--check mode
meta: fix tc classid parsing out-of-bounds access
netlink: don't crash if prefix for < byte is requested
netlink: fix leaking typeof_expr_data/typeof_expr_key in netlink_delinearize_set()
netlink: handle invalid etype in set_make_key()
parser_bison: fix ct scope underflow if ct helper section is duplicated
parser_bison: fix memleak in meta set error handling
parser_bison: make sure obj_free releases timeout policies
parser_json: Catch nonsense ops in match statement
parser_json: Default meter size to zero
parser_json: Fix flowtable prio value parsing
parser_json: Fix synproxy object mss/wscale parsing
parser_json: Fix typo in json_parse_cmd_add_object()
parser_json: Proper ct expectation attribute parsing
parser_json: Wrong check in json_parse_ct_timeout_policy()
py: fix exception during cleanup of half-initialized Nftables
rule: fix sym refcount assertion

* Wed Mar 13 2024 zhouyihang <zhouyihang3@h-partners.com> - 1:1.0.8-2
- Type: bugfix
- ID: NA
- SUG: NA
- DESC: create standlone module dir to fix import error

* Sat Jan 20 2024 zhanghao <zhanghao383@huawei.com> - 1:1.0.8-1
- Type: requirement
- ID: NA
- SUG: NA
- DESC: update to version 1.0.8

* Mon Nov 06 2023 liweigang <weigangli99@gmail.com> - 1:1.0.7-1
- Type: requirement
- ID: NA
- SUG: NA
- DESC: update to version 1.0.7

* Wed Feb 15 2023 zhanghao <zhanghao383@huawei.com> - 1:1.0.5-2
- Type:requirement
- ID:NA
- SUG:NA
- DESC:fix one patch from 1.0.0 and delete useless Patches

* Wed Feb 08 2023 zhanghao <zhanghao383@huawei.com> - 1:1.0.5-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update to 1.0.5

* Mon Nov 21 2022 huangyu <huangyu106@huawei.com> - 1:1.0.0-4
- Type:feature
- ID:NA
- SUG:NA
- DESC:enabled DT testcase

* Sat Sep 03 2022 xinghe <xinghe2@h-partners.com> - 1:1.0.0-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix cache prepare nft_cache evaluate to return error
       fix cache validate handle string length
       add src support for implicit chain bindings
       fix cache release pending rules
       fix segtree map listing
       parser_json fix device parsing in netdev family
       fix src Don't parse string as verdict in map

* Mon Aug 1 2022 huangyu <huangyu106@huawei.com> - 1:1.0.0-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:The python-setup tools causes an error in the nftables packaging path,This macro is added to ensure that path remains unchanged

* Sat Mar 19 2022 quanhongfei <quanhongfei@h-partners.com> - 1:1.0.0-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update nftables to 1.0.0

* Tue Sep 07 2021 gaihuiying <gaihuiying1@huawei.com> - 1:0.9.9-3
- Type:requirement
- ID:NA
- SUG:NA
- DESC:remove rpath of nft

* Tue Aug 24 2021 gaihuiying <gaihuiying1@huawei.com> - 1:0.9.9-2
- json: fix base chain output 

* Fri Jul 23 2021 gaihuiying <gaihuiying1@huawei.com> - 1:0.9.9-1
- update to 0.9.9

* Thu Jul 30 2020 cuibaobao <buildteam@openeuler.org> - 1:0.9.6-2
- Add python3-nftables sub-package

* Thu Jul 23 2020 cuibaobao <buildteam@openeuler.org> - 1:0.9.6-1
- update to 0.9.6

* Tue Sep 17 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:0.9.0-3
- Package init
